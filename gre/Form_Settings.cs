﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace gre
{
    public partial class Form_Settings : Form
    {
        public Settings settings;
        private Form1 ownerFrm;
        public Form_Settings(Settings settings,Form1 ownerFrm)
        {
            this.settings = settings;
            this.ownerFrm = ownerFrm;
            InitializeComponent();
        }

        private void Form_Settings_Load(object sender, EventArgs e)
        {
            this.settingsBindingSource.DataSource = this.settings;
        }

        private void OK_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void opacityTrackBar_ValueChanged(object sender, EventArgs e)
        {
            ownerFrm.Opacity = (float)this.opacityTrackBar.Value / 100;
        }

        private void OK_Cancel(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}

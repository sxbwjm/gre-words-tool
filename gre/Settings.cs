﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;

namespace gre
{
    public class Settings
    {
        public int no { get; set; }
        public int level {get;set;}
        public string mode { get; set; }
        public float opacity { get; set; }

        public int interval { get; set; }
        public int no_disp_interval { get; set; }

        public int known_interval { get; set; }
        public int known_no_disp_interval { get; set; }
        
        public int review_known_interval { get; set; }
        public int review_unknown_interval { get; set; }


        public System.Drawing.Point dispPos { get; set; }

        public void SaveSettings()
        {
            FileStream stream = new FileStream("setting.xml", FileMode.Create);
            try
            {
                XmlSerializer xmlWriter = new XmlSerializer(typeof(Settings));
                xmlWriter.Serialize(stream, this);
            }
            finally
            {
                stream.Close();
            }
        }

        static public Settings ReadSettings()
        {
            Settings settings;
            FileStream stream;
            try
            {
                stream = new FileStream("setting.xml", FileMode.Open);
            }
            catch (FileNotFoundException ex)
            {
                settings = new Settings();
                return settings;
            }
            try
            {
                XmlSerializer xmlReader = new XmlSerializer(typeof(Settings));
                settings = (Settings)xmlReader.Deserialize(stream);
            }
            catch (Exception ex)
            {
                settings = new Settings();
            }
            finally
            {
                stream.Close();
            }
            return settings;
        }

    }
}

﻿namespace gre
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナ変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナで生成されたコード

        /// <summary>
        /// デザイナ サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディタで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.word_spellLabel1 = new System.Windows.Forms.Label();
            this.wordsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.greDataSet = new gre.greDataSet();
            this.pronLabel1 = new System.Windows.Forms.Label();
            this.meaningLabel1 = new System.Windows.Forms.Label();
            this.noLabel1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.levelLabel1 = new System.Windows.Forms.Label();
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.wordsTableAdapter = new gre.greDataSetTableAdapters.wordsTableAdapter();
            this.tableAdapterManager = new gre.greDataSetTableAdapters.TableAdapterManager();
            this.toolTip_meaning = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.wordsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.greDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // word_spellLabel1
            // 
            this.word_spellLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wordsBindingSource, "word_spell", true));
            this.word_spellLabel1.Font = new System.Drawing.Font("MS UI Gothic", 9F);
            this.word_spellLabel1.Location = new System.Drawing.Point(72, 11);
            this.word_spellLabel1.Name = "word_spellLabel1";
            this.word_spellLabel1.Size = new System.Drawing.Size(100, 20);
            this.word_spellLabel1.TabIndex = 4;
            this.word_spellLabel1.Text = "label1";
            this.word_spellLabel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.word_spellLabel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.word_spellLabel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // wordsBindingSource
            // 
            this.wordsBindingSource.DataMember = "words";
            this.wordsBindingSource.DataSource = this.greDataSet;
            this.wordsBindingSource.BindingComplete += new System.Windows.Forms.BindingCompleteEventHandler(this.wordsBindingSource_BindingComplete);
            // 
            // greDataSet
            // 
            this.greDataSet.DataSetName = "greDataSet";
            this.greDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pronLabel1
            // 
            this.pronLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wordsBindingSource, "pron", true));
            this.pronLabel1.Font = new System.Drawing.Font("Kingsoft Phonetic Plain", 9F);
            this.pronLabel1.Location = new System.Drawing.Point(178, 11);
            this.pronLabel1.Name = "pronLabel1";
            this.pronLabel1.Size = new System.Drawing.Size(100, 20);
            this.pronLabel1.TabIndex = 6;
            this.pronLabel1.Text = "label1";
            this.pronLabel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.pronLabel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.pronLabel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // meaningLabel1
            // 
            this.meaningLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wordsBindingSource, "meaning", true));
            this.meaningLabel1.Location = new System.Drawing.Point(296, 11);
            this.meaningLabel1.Name = "meaningLabel1";
            this.meaningLabel1.Size = new System.Drawing.Size(350, 20);
            this.meaningLabel1.TabIndex = 8;
            this.meaningLabel1.Text = "label1";
            this.meaningLabel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.meaningLabel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.meaningLabel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // noLabel1
            // 
            this.noLabel1.BackColor = System.Drawing.Color.Transparent;
            this.noLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wordsBindingSource, "no", true));
            this.noLabel1.Location = new System.Drawing.Point(12, 11);
            this.noLabel1.Name = "noLabel1";
            this.noLabel1.Size = new System.Drawing.Size(50, 20);
            this.noLabel1.TabIndex = 11;
            this.noLabel1.Text = "label1";
            this.noLabel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.noLabel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.noLabel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // timer1
            // 
            this.timer1.Interval = 4000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 2000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // levelLabel1
            // 
            this.levelLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wordsBindingSource, "level", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "N0"));
            this.levelLabel1.Location = new System.Drawing.Point(652, 12);
            this.levelLabel1.Name = "levelLabel1";
            this.levelLabel1.Size = new System.Drawing.Size(12, 22);
            this.levelLabel1.TabIndex = 12;
            // 
            // timer3
            // 
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // wordsTableAdapter
            // 
            this.wordsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.UpdateOrder = gre.greDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.wordsTableAdapter = this.wordsTableAdapter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 43);
            this.Controls.Add(this.levelLabel1);
            this.Controls.Add(this.noLabel1);
            this.Controls.Add(this.word_spellLabel1);
            this.Controls.Add(this.pronLabel1);
            this.Controls.Add(this.meaningLabel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Opacity = 0.2;
            this.ShowInTaskbar = false;
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.Transparent;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.wordsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.greDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource wordsBindingSource;
        private gre.greDataSetTableAdapters.wordsTableAdapter wordsTableAdapter;
        private gre.greDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Label word_spellLabel1;
        private System.Windows.Forms.Label pronLabel1;
        private System.Windows.Forms.Label meaningLabel1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label levelLabel1;
        private System.Windows.Forms.Timer timer3;
        internal System.Windows.Forms.Label noLabel1;
        internal greDataSet greDataSet;
        private System.Windows.Forms.ToolTip toolTip_meaning;

    }
}


﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gre
{
    public class Word
    {
        public int no { set; get; }
        public string word_spell { set; get; }
        public string pron {set;get;}
        public string meaning{set;get;}
        public int level {set;get;}
    }
}

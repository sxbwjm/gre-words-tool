﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using System.Data.OleDb;


namespace gre
{
    static class DataReadWrite
    {
        /// <summary>
        /// データセットをエクセルファイルへ保存
        /// </summary>
        /// <param name="ds">データセット</param>
        /// <param name="tableNameMapping">テーブル名のマップィング(データセット.テーブル名⇒エクセルシート名</param>
        /// <param name="strExcelFileName">エクセルファイル名(フルパス)</param>
        static public void SaveDataSetToExcel(DataSet ds, DataTable tableNameMapping, string strExcelFileName)
        {
            if (File.Exists(strExcelFileName))
            {
                File.Delete(strExcelFileName);
            }
            //EXCELL接続用文字列
            String sConnectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=Excel 8.0;", strExcelFileName);

            // 接続オブジェクト作成
            OleDbConnection objConn = new OleDbConnection(sConnectionString);
            // 接続
            objConn.Open();
            OleDbCommand objCmd = new OleDbCommand();
            objCmd.Connection = objConn;

            string strTableName;
            string strSql;
            foreach (DataTable dt in ds.Tables)
            {
                /* テーブル作成 */
                //データセットのテーブル名(フォーマットコード)により日本語名称を取得
                strTableName = tableNameMapping == null ? dt.TableName : tableNameMapping.Rows.Find(dt.TableName)["FormatName"].ToString();
                //エクセルシートと列定義を作成(連番列(NO)付き)
                strSql = string.Format(@"CREATE TABLE {0}(ＮＯ text,", strTableName);
                //列定義
                foreach (DataColumn dc in dt.Columns)
                {
                    strSql += string.Format("{0} text,", dc.ColumnName);
                }
                //最後の余分の","を削除
                strSql = strSql.TrimEnd(',');
                strSql += ");";
                objCmd.CommandText = strSql;
                objCmd.ExecuteNonQuery();
                /*データエクスポート*/
                int intNo = 1;
                foreach (DataRow dr in dt.Rows)
                {
                    strSql = string.Format(@"INSERT INTO [{0}$] VALUES('{1}',", strTableName, intNo.ToString());
                    foreach (object value in dr.ItemArray)
                    {
                        strSql += string.Format("'{0}',", value.ToString());
                    }
                    strSql = strSql.TrimEnd(',');
                    strSql += ");";
                    objCmd.CommandText = strSql;
                    objCmd.ExecuteNonQuery();
                    intNo++;
                }
            }

            objConn.Close();
        }

        /// <summary>
        /// データテーブルをエクセルファイルへ保存
        /// </summary>
        /// <param name="ds">データセット</param>
        /// <param name="tableNameMapping">テーブル名のマップィング(データセット.テーブル名⇒エクセルシート名</param>
        /// <param name="strExcelFileName">エクセルファイル名(フルパス)</param>
        static public void SaveDataSetToExcel(DataTable dt, string strTableName, string strExcelFileName)
        {
            if (File.Exists(strExcelFileName))
            {
                File.Delete(strExcelFileName);
            }
            //EXCELL接続用文字列
            String sConnectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=Excel 8.0;", strExcelFileName);

            // 接続オブジェクト作成
            OleDbConnection objConn = new OleDbConnection(sConnectionString);
            // 接続
            objConn.Open();
            OleDbCommand objCmd = new OleDbCommand();
            objCmd.Connection = objConn;

            string strSql;
            /* テーブル作成 */
            //エクセルシートと列定義を作成(連番列(NO)付き)
            strSql = string.Format(@"CREATE TABLE {0}(ＮＯ text,", strTableName);
            //列定義
            foreach (DataColumn dc in dt.Columns)
            {
                strSql += string.Format("{0} text,", "col_" + dc.ColumnName);
            }
            //最後の余分の","を削除
            strSql = strSql.TrimEnd(',');
            strSql += ");";
            objCmd.CommandText = strSql;
            objCmd.ExecuteNonQuery();
            /*データエクスポート*/
            int intNo = 1;
            foreach (DataRow dr in dt.Rows)
            {
                strSql = string.Format(@"INSERT INTO [{0}$] VALUES('{1}',", strTableName, intNo.ToString());
                foreach (object value in dr.ItemArray)
                {
                    strSql += string.Format("'{0}',", value.ToString().Replace("'","''"));
                }
                strSql = strSql.TrimEnd(',');
                strSql += ");";
                objCmd.CommandText = strSql;
                objCmd.ExecuteNonQuery();
                intNo++;
            }

            objConn.Close();
        }

        /// <summary>
        /// データビューをエクセルファイルへ保存
        /// </summary>
        /// <param name="ds">データセット</param>
        /// <param name="tableNameMapping">テーブル名のマップィング(データセット.テーブル名⇒エクセルシート名</param>
        /// <param name="strExcelFileName">エクセルファイル名(フルパス)</param>
        static public void SaveDataSetToExcel(DataView dv, string strTableName, string strExcelFileName)
        {
            DataTable dt = dv.Table;

            if (File.Exists(strExcelFileName))
            {
                File.Delete(strExcelFileName);
            }
            //EXCELL接続用文字列
            String sConnectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=Excel 8.0;", strExcelFileName);

            // 接続オブジェクト作成
            OleDbConnection objConn = new OleDbConnection(sConnectionString);
            // 接続
            objConn.Open();
            OleDbCommand objCmd = new OleDbCommand();
            objCmd.Connection = objConn;

            string strSql;
            /* テーブル作成 */
            //エクセルシートと列定義を作成(連番列(NO)付き)
            strSql = string.Format(@"CREATE TABLE {0}(ＮＯ text,", strTableName);
            //列定義
            foreach (DataColumn dc in dt.Columns)
            {
                strSql += string.Format("{0} text,", "col_" + dc.ColumnName);
            }
            //最後の余分の","を削除
            strSql = strSql.TrimEnd(',');
            strSql += ");";
            objCmd.CommandText = strSql;
            objCmd.ExecuteNonQuery();
            /*データエクスポート*/
            int intNo = 1;
            foreach (DataRowView drv in dv)
            {
                strSql = string.Format(@"INSERT INTO [{0}$] VALUES('{1}',", strTableName, intNo.ToString());
                foreach (object value in drv.Row.ItemArray)
                {
                    strSql += string.Format("'{0}',", value.ToString().Replace("'", "''"));
                }
                strSql = strSql.TrimEnd(',');
                strSql += ");";
                objCmd.CommandText = strSql;
                objCmd.ExecuteNonQuery();
                intNo++;
            }

            objConn.Close();
        }
    }
}

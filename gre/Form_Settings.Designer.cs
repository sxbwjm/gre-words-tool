﻿namespace gre
{
    partial class Form_Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label intervalLabel;
            System.Windows.Forms.Label levelLabel;
            System.Windows.Forms.Label noLabel;
            System.Windows.Forms.Label no_disp_intervalLabel;
            System.Windows.Forms.Label opacityLabel;
            System.Windows.Forms.Label modeLabel;
            System.Windows.Forms.Label known_intervalLabel;
            System.Windows.Forms.Label known_no_disp_intervalLabel;
            this.intervalTextBox = new System.Windows.Forms.TextBox();
            this.levelTextBox = new System.Windows.Forms.TextBox();
            this.noTextBox = new System.Windows.Forms.TextBox();
            this.OK = new System.Windows.Forms.Button();
            this.no_disp_intervalTextBox = new System.Windows.Forms.TextBox();
            this.opacityTrackBar = new System.Windows.Forms.TrackBar();
            this.button_cancel = new System.Windows.Forms.Button();
            this.modeComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.settingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.known_intervalTextBox = new System.Windows.Forms.TextBox();
            this.known_no_disp_intervalTextBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            intervalLabel = new System.Windows.Forms.Label();
            levelLabel = new System.Windows.Forms.Label();
            noLabel = new System.Windows.Forms.Label();
            no_disp_intervalLabel = new System.Windows.Forms.Label();
            opacityLabel = new System.Windows.Forms.Label();
            modeLabel = new System.Windows.Forms.Label();
            known_intervalLabel = new System.Windows.Forms.Label();
            known_no_disp_intervalLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.opacityTrackBar)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsBindingSource)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // intervalLabel
            // 
            intervalLabel.AutoSize = true;
            intervalLabel.Location = new System.Drawing.Point(15, 32);
            intervalLabel.Name = "intervalLabel";
            intervalLabel.Size = new System.Drawing.Size(59, 12);
            intervalLabel.TabIndex = 1;
            intervalLabel.Text = "interval:";
            // 
            // levelLabel
            // 
            levelLabel.AutoSize = true;
            levelLabel.Location = new System.Drawing.Point(150, 18);
            levelLabel.Name = "levelLabel";
            levelLabel.Size = new System.Drawing.Size(41, 12);
            levelLabel.TabIndex = 3;
            levelLabel.Text = "level:";
            // 
            // noLabel
            // 
            noLabel.AutoSize = true;
            noLabel.Location = new System.Drawing.Point(21, 15);
            noLabel.Name = "noLabel";
            noLabel.Size = new System.Drawing.Size(23, 12);
            noLabel.TabIndex = 5;
            noLabel.Text = "no:";
            // 
            // no_disp_intervalLabel
            // 
            no_disp_intervalLabel.AutoSize = true;
            no_disp_intervalLabel.Location = new System.Drawing.Point(144, 32);
            no_disp_intervalLabel.Name = "no_disp_intervalLabel";
            no_disp_intervalLabel.Size = new System.Drawing.Size(107, 12);
            no_disp_intervalLabel.TabIndex = 7;
            no_disp_intervalLabel.Text = "no disp interval:";
            // 
            // opacityLabel
            // 
            opacityLabel.AutoSize = true;
            opacityLabel.Location = new System.Drawing.Point(22, 204);
            opacityLabel.Name = "opacityLabel";
            opacityLabel.Size = new System.Drawing.Size(53, 12);
            opacityLabel.TabIndex = 9;
            opacityLabel.Text = "opacity:";
            // 
            // modeLabel
            // 
            modeLabel.AutoSize = true;
            modeLabel.Location = new System.Drawing.Point(25, 254);
            modeLabel.Name = "modeLabel";
            modeLabel.Size = new System.Drawing.Size(35, 12);
            modeLabel.TabIndex = 13;
            modeLabel.Text = "mode:";
            // 
            // intervalTextBox
            // 
            this.intervalTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.settingsBindingSource, "interval", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "N1"));
            this.intervalTextBox.Location = new System.Drawing.Point(79, 29);
            this.intervalTextBox.Name = "intervalTextBox";
            this.intervalTextBox.Size = new System.Drawing.Size(51, 21);
            this.intervalTextBox.TabIndex = 2;
            // 
            // levelTextBox
            // 
            this.levelTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.settingsBindingSource, "level", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "N0"));
            this.levelTextBox.Location = new System.Drawing.Point(198, 15);
            this.levelTextBox.Name = "levelTextBox";
            this.levelTextBox.Size = new System.Drawing.Size(68, 21);
            this.levelTextBox.TabIndex = 3;
            // 
            // noTextBox
            // 
            this.noTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.settingsBindingSource, "no", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "N0"));
            this.noTextBox.Location = new System.Drawing.Point(64, 12);
            this.noTextBox.Name = "noTextBox";
            this.noTextBox.Size = new System.Drawing.Size(68, 21);
            this.noTextBox.TabIndex = 1;
            // 
            // OK
            // 
            this.OK.Location = new System.Drawing.Point(182, 245);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(75, 37);
            this.OK.TabIndex = 5;
            this.OK.Text = "OK";
            this.OK.UseVisualStyleBackColor = true;
            this.OK.Click += new System.EventHandler(this.OK_Click);
            // 
            // no_disp_intervalTextBox
            // 
            this.no_disp_intervalTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.settingsBindingSource, "no_disp_interval", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "N1"));
            this.no_disp_intervalTextBox.Location = new System.Drawing.Point(257, 29);
            this.no_disp_intervalTextBox.Name = "no_disp_intervalTextBox";
            this.no_disp_intervalTextBox.Size = new System.Drawing.Size(43, 21);
            this.no_disp_intervalTextBox.TabIndex = 4;
            // 
            // opacityTrackBar
            // 
            this.opacityTrackBar.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.settingsBindingSource, "opacity", true));
            this.opacityTrackBar.Location = new System.Drawing.Point(76, 204);
            this.opacityTrackBar.Maximum = 100;
            this.opacityTrackBar.Name = "opacityTrackBar";
            this.opacityTrackBar.Size = new System.Drawing.Size(247, 45);
            this.opacityTrackBar.TabIndex = 13;
            this.opacityTrackBar.ValueChanged += new System.EventHandler(this.opacityTrackBar_ValueChanged);
            // 
            // button_cancel
            // 
            this.button_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button_cancel.Location = new System.Drawing.Point(263, 245);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(75, 37);
            this.button_cancel.TabIndex = 5;
            this.button_cancel.Text = "Cancel";
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.OK_Cancel);
            // 
            // modeComboBox
            // 
            this.modeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.settingsBindingSource, "mode", true));
            this.modeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.modeComboBox.FormattingEnabled = true;
            this.modeComboBox.Items.AddRange(new object[] {
            "home",
            "office"});
            this.modeComboBox.Location = new System.Drawing.Point(76, 251);
            this.modeComboBox.Name = "modeComboBox";
            this.modeComboBox.Size = new System.Drawing.Size(98, 20);
            this.modeComboBox.TabIndex = 14;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(known_intervalLabel);
            this.groupBox1.Controls.Add(this.known_intervalTextBox);
            this.groupBox1.Controls.Add(intervalLabel);
            this.groupBox1.Controls.Add(this.intervalTextBox);
            this.groupBox1.Controls.Add(this.no_disp_intervalTextBox);
            this.groupBox1.Controls.Add(no_disp_intervalLabel);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(15, 58);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(317, 118);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "display interval";
            // 
            // settingsBindingSource
            // 
            this.settingsBindingSource.DataSource = typeof(gre.Settings);
            // 
            // known_intervalLabel
            // 
            known_intervalLabel.AutoSize = true;
            known_intervalLabel.Location = new System.Drawing.Point(15, 81);
            known_intervalLabel.Name = "known_intervalLabel";
            known_intervalLabel.Size = new System.Drawing.Size(59, 12);
            known_intervalLabel.TabIndex = 7;
            known_intervalLabel.Text = "interval:";
            // 
            // known_intervalTextBox
            // 
            this.known_intervalTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.settingsBindingSource, "known_interval", true));
            this.known_intervalTextBox.Location = new System.Drawing.Point(80, 78);
            this.known_intervalTextBox.Name = "known_intervalTextBox";
            this.known_intervalTextBox.Size = new System.Drawing.Size(50, 21);
            this.known_intervalTextBox.TabIndex = 8;
            // 
            // known_no_disp_intervalLabel
            // 
            known_no_disp_intervalLabel.AutoSize = true;
            known_no_disp_intervalLabel.Location = new System.Drawing.Point(136, 25);
            known_no_disp_intervalLabel.Name = "known_no_disp_intervalLabel";
            known_no_disp_intervalLabel.Size = new System.Drawing.Size(107, 12);
            known_no_disp_intervalLabel.TabIndex = 8;
            known_no_disp_intervalLabel.Text = "no disp interval:";
            // 
            // known_no_disp_intervalTextBox
            // 
            this.known_no_disp_intervalTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.settingsBindingSource, "known_no_disp_interval", true));
            this.known_no_disp_intervalTextBox.Location = new System.Drawing.Point(249, 25);
            this.known_no_disp_intervalTextBox.Name = "known_no_disp_intervalTextBox";
            this.known_no_disp_intervalTextBox.Size = new System.Drawing.Size(43, 21);
            this.known_no_disp_intervalTextBox.TabIndex = 9;
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(8, 14);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(300, 42);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Known";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.known_no_disp_intervalTextBox);
            this.groupBox3.Controls.Add(known_no_disp_intervalLabel);
            this.groupBox3.Location = new System.Drawing.Point(8, 56);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(300, 56);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Known";
            // 
            // Form_Settings
            // 
            this.AcceptButton = this.OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button_cancel;
            this.ClientSize = new System.Drawing.Size(362, 300);
            this.Controls.Add(modeLabel);
            this.Controls.Add(this.modeComboBox);
            this.Controls.Add(this.opacityTrackBar);
            this.Controls.Add(opacityLabel);
            this.Controls.Add(this.button_cancel);
            this.Controls.Add(this.OK);
            this.Controls.Add(levelLabel);
            this.Controls.Add(this.levelTextBox);
            this.Controls.Add(noLabel);
            this.Controls.Add(this.noTextBox);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form_Settings";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form_Settings";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Form_Settings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.opacityTrackBar)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsBindingSource)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource settingsBindingSource;
        private System.Windows.Forms.TextBox intervalTextBox;
        private System.Windows.Forms.TextBox levelTextBox;
        private System.Windows.Forms.TextBox noTextBox;
        private System.Windows.Forms.Button OK;
        private System.Windows.Forms.TextBox no_disp_intervalTextBox;
        private System.Windows.Forms.TrackBar opacityTrackBar;
        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.ComboBox modeComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox known_no_disp_intervalTextBox;
        private System.Windows.Forms.TextBox known_intervalTextBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}
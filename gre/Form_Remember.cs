﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace gre
{
    public partial class Form_Remember : Form
    {
        Form1 parenForm;
        public Form_Remember(Form1 parenForm)
        {
            this.parenForm = parenForm;
            InitializeComponent();
        }

        private void Form_Remember_Load(object sender, EventArgs e)
        {
            // TODO: このコード行はデータを 'greDataSet.remember' テーブルに読み込みます。必要に応じて移動、または削除をしてください。
            this.rememberTableAdapter.Fill(this.greDataSet.remember);

        }

        private void Form_Remember_Shown(object sender, EventArgs e)
        {
            int intAll,intKnown,intUnKnown, intNow;
            intAll = this.parenForm.greDataSet.words.Count;
            intNow = int.Parse(this.parenForm.noLabel1.Text);
            intKnown = int.Parse((this.parenForm.greDataSet.words.Compute("sum(level)",string.Format("no <= {0:d}",intNow))).ToString());
            intUnKnown = intNow - intKnown;

            this.num_allLabel1.Text = intAll.ToString();
            this.label_now.Text = intNow.ToString();
            this.num_knownLabel1.Text = intKnown.ToString();
            this.num_unkonwnLabel1.Text = intUnKnown.ToString();

            this.progressBar1.Maximum = intAll;
            this.progressBar1.Minimum = 0;
            this.progressBar1.Value = intNow;
            this.label_ratio.Text = string.Format("{0:f2}%",((float)this.progressBar1.Value / this.progressBar1.Maximum) * 100);

            this.progressBar2.Maximum = intNow;
            this.progressBar2.Minimum = 0;
            this.progressBar2.Value = intKnown;
            this.label_ratio2.Text = string.Format("{0:f2}%",((float)this.progressBar2.Value / this.progressBar2.Maximum) * 100);
        }

        private void Form_Remember_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                this.Dispose();
            }
        }

        private void Save_Click(object sender, EventArgs e)
        {
            this.Opacity = 0.2;
            DataView dVwordsData;
            dVwordsData = this.parenForm.greDataSet.words.DefaultView;
            dVwordsData.RowFilter = "level = 0";
            DataReadWrite.SaveDataSetToExcel(dVwordsData, "words", "words.xls");
            this.Opacity = 1;
            MessageBox.Show("Complete!");
            this.Dispose();
        }
    }
}

﻿namespace gre
{
    partial class Form_Remember
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label num_allLabel;
            System.Windows.Forms.Label num_knownLabel;
            System.Windows.Forms.Label num_unkonwnLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            this.num_allLabel1 = new System.Windows.Forms.Label();
            this.num_knownLabel1 = new System.Windows.Forms.Label();
            this.num_unkonwnLabel1 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label_now = new System.Windows.Forms.Label();
            this.label_ratio = new System.Windows.Forms.Label();
            this.rememberBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.greDataSet = new gre.greDataSet();
            this.rememberTableAdapter = new gre.greDataSetTableAdapters.rememberTableAdapter();
            this.tableAdapterManager = new gre.greDataSetTableAdapters.TableAdapterManager();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.label_ratio2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Save = new System.Windows.Forms.Button();
            num_allLabel = new System.Windows.Forms.Label();
            num_knownLabel = new System.Windows.Forms.Label();
            num_unkonwnLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.rememberBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.greDataSet)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // num_allLabel
            // 
            num_allLabel.AutoSize = true;
            num_allLabel.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            num_allLabel.Location = new System.Drawing.Point(7, 4);
            num_allLabel.Name = "num_allLabel";
            num_allLabel.Size = new System.Drawing.Size(28, 12);
            num_allLabel.TabIndex = 1;
            num_allLabel.Text = "ALL";
            // 
            // num_knownLabel
            // 
            num_knownLabel.AutoSize = true;
            num_knownLabel.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            num_knownLabel.Location = new System.Drawing.Point(209, 4);
            num_knownLabel.Name = "num_knownLabel";
            num_knownLabel.Size = new System.Drawing.Size(50, 12);
            num_knownLabel.TabIndex = 3;
            num_knownLabel.Text = "KNOWN";
            // 
            // num_unkonwnLabel
            // 
            num_unkonwnLabel.AutoSize = true;
            num_unkonwnLabel.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            num_unkonwnLabel.Location = new System.Drawing.Point(310, 4);
            num_unkonwnLabel.Name = "num_unkonwnLabel";
            num_unkonwnLabel.Size = new System.Drawing.Size(68, 12);
            num_unkonwnLabel.TabIndex = 5;
            num_unkonwnLabel.Text = "UNKNOWN";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            label1.Location = new System.Drawing.Point(108, 4);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(33, 12);
            label1.TabIndex = 3;
            label1.Text = "NOW";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            label2.Location = new System.Drawing.Point(9, 64);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(61, 12);
            label2.TabIndex = 1;
            label2.Text = "Progress:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            label3.Location = new System.Drawing.Point(9, 82);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(81, 12);
            label3.TabIndex = 1;
            label3.Text = "Remembered";
            // 
            // num_allLabel1
            // 
            this.num_allLabel1.Location = new System.Drawing.Point(7, 21);
            this.num_allLabel1.Name = "num_allLabel1";
            this.num_allLabel1.Size = new System.Drawing.Size(40, 23);
            this.num_allLabel1.TabIndex = 2;
            this.num_allLabel1.Text = "label1";
            // 
            // num_knownLabel1
            // 
            this.num_knownLabel1.Location = new System.Drawing.Point(209, 21);
            this.num_knownLabel1.Name = "num_knownLabel1";
            this.num_knownLabel1.Size = new System.Drawing.Size(38, 23);
            this.num_knownLabel1.TabIndex = 4;
            this.num_knownLabel1.Text = "label1";
            // 
            // num_unkonwnLabel1
            // 
            this.num_unkonwnLabel1.Location = new System.Drawing.Point(310, 21);
            this.num_unkonwnLabel1.Name = "num_unkonwnLabel1";
            this.num_unkonwnLabel1.Size = new System.Drawing.Size(46, 23);
            this.num_unkonwnLabel1.TabIndex = 6;
            this.num_unkonwnLabel1.Text = "label1";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(96, 57);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(274, 19);
            this.progressBar1.TabIndex = 7;
            // 
            // label_now
            // 
            this.label_now.AutoSize = true;
            this.label_now.Location = new System.Drawing.Point(108, 21);
            this.label_now.Name = "label_now";
            this.label_now.Size = new System.Drawing.Size(41, 12);
            this.label_now.TabIndex = 8;
            this.label_now.Text = "label2";
            // 
            // label_ratio
            // 
            this.label_ratio.AutoSize = true;
            this.label_ratio.Location = new System.Drawing.Point(374, 57);
            this.label_ratio.Name = "label_ratio";
            this.label_ratio.Size = new System.Drawing.Size(41, 12);
            this.label_ratio.TabIndex = 9;
            this.label_ratio.Text = "label2";
            // 
            // rememberBindingSource
            // 
            this.rememberBindingSource.DataMember = "remember";
            this.rememberBindingSource.DataSource = this.greDataSet;
            // 
            // greDataSet
            // 
            this.greDataSet.DataSetName = "greDataSet";
            this.greDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rememberTableAdapter
            // 
            this.rememberTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = gre.greDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.wordsTableAdapter = null;
            // 
            // progressBar2
            // 
            this.progressBar2.Location = new System.Drawing.Point(96, 82);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(274, 20);
            this.progressBar2.TabIndex = 10;
            // 
            // label_ratio2
            // 
            this.label_ratio2.AutoSize = true;
            this.label_ratio2.Location = new System.Drawing.Point(374, 82);
            this.label_ratio2.Name = "label_ratio2";
            this.label_ratio2.Size = new System.Drawing.Size(41, 12);
            this.label_ratio2.TabIndex = 9;
            this.label_ratio2.Text = "label2";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(num_allLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(label1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(num_unkonwnLabel, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label_now, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.num_allLabel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.num_unkonwnLabel1, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.num_knownLabel1, 2, 1);
            this.tableLayoutPanel1.Controls.Add(num_knownLabel, 2, 0);
            this.tableLayoutPanel1.ForeColor = System.Drawing.SystemColors.Window;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40.42553F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 59.57447F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(412, 48);
            this.tableLayoutPanel1.TabIndex = 11;
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(85, 115);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(256, 33);
            this.Save.TabIndex = 12;
            this.Save.TabStop = false;
            this.Save.Text = "save";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // Form_Remember
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(420, 150);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.progressBar2);
            this.Controls.Add(this.label_ratio2);
            this.Controls.Add(this.label_ratio);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(label3);
            this.Controls.Add(label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form_Remember";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form_Remember";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Form_Remember_Load);
            this.Shown += new System.EventHandler(this.Form_Remember_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form_Remember_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.rememberBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.greDataSet)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private greDataSet greDataSet;
        private System.Windows.Forms.BindingSource rememberBindingSource;
        private gre.greDataSetTableAdapters.rememberTableAdapter rememberTableAdapter;
        private gre.greDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Label num_allLabel1;
        private System.Windows.Forms.Label num_knownLabel1;
        private System.Windows.Forms.Label num_unkonwnLabel1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label_now;
        private System.Windows.Forms.Label label_ratio;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.Label label_ratio2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button Save;

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Runtime.InteropServices;



namespace gre
{
    public partial class Form1 : Form
    {
        private const int COMPONENT_INTERVAL = 10;
       
        //style of form
        private const int OFFICE_COMPONENT_Y_POSITION = 5;
        private const float OFFICE_FONT_SIZE = 9;
        private Color OFFICE_FORM_COLOR = SystemColors.Control;
        private Color OFFICE_FONT_COLOR = SystemColors.ControlText;

        private const int OFFICE_FORM_SIZE_WITH = 670;
        private const int OFFICE_FORM_SIZE_HEIGHT = 40;
        private const int OFFICE_LABLE_NO_SIZE_WITH = 50;
        private const int OFFICE_LABLE_NO_SIZE_HEIGHT = 30;
        private const int OFFICE_LABLE_WORD_SIZE_WITH = 100;
        private const int OFFICE_LABLE_WORD_SIZE_HEIGHT = 30;
        private const int OFFICE_LABLE_PRON_SIZE_WITH = 100;
        private const int OFFICE_LABLE_PRON_SIZE_HEIGHT = 30;
        private const int OFFICE_LABLE_MEANING_SIZE_WITH = 350;
        private const int OFFICE_LABLE_MEANING_SIZE_HEIGHT = 30;
        private const int OFFICE_LABLE_LEVEL_SIZE_WITH = 10;
        private const int OFFICE_LABLE_LEVEL_SIZE_HEIGHT = 30;

        private const int HOME_COMPONENT_Y_POSITION = 10;
        private const float HOME_FONT_SIZE = 18;
        private Color HOME_FORM_COLOR = SystemColors.Highlight;
        private  Color HOME_FONT_COLOR = Color.White;

        private const int HOME_FORM_SIZE_WITH = 1240;
        private const int HOME_FORM_SIZE_HEIGHT = 80;
        private const int HOME_LABLE_NO_SIZE_WITH = 100;
        private const int HOME_LABLE_NO_SIZE_HEIGHT = 60;
        private const int HOME_LABLE_WORD_SIZE_WITH = 200;
        private const int HOME_LABLE_WORD_SIZE_HEIGHT = 60;
        private const int HOME_LABLE_PRON_SIZE_WITH = 200;
        private const int HOME_LABLE_PRON_SIZE_HEIGHT = 60;
        private const int HOME_LABLE_MEANING_SIZE_WITH = 650;
        private const int HOME_LABLE_MEANING_SIZE_HEIGHT = 60;
        private const int HOME_LABLE_LEVEL_SIZE_WITH = 30;
        private const int HOME_LABLE_LEVEL_SIZE_HEIGHT = 60;

        //display control
        private const int DEFAULT_INTERVAL = 3;
        private const int DEFAULT_NO_DISP_INTERVAL = 2;

        private const float DEFAULT_INTERVAL_REVIEW_KNOWN = 0.5f;
        private const int DEFAULT_INTERVAL_REVIEW_UNKNOWN = 3;

        //

        private const int START_POSITION = 1;

        private Point mousePoint = new Point();
        public Settings settings = new Settings();
        public Form_Settings frmSettings;
        public Form_Remember frmRemember;
        public DataView dVwordsData;

        private float tmpFontSize;

        //the mode of study: 1 - study  (space to start/stop the auto slide)  
        //                   2 - review ( space: known enter:unkown)
        private int intStudyMode = 1;

        private bool blMeaningToolTipDispFlg = false;


         //
        // モディファイアキー
        //
        const int MOD_NONE = 0x0000;
        const int MOD_ALT     = 0x0001;
        const int MOD_CONTROL = 0x0002;
        const int MOD_SHIFT   = 0x0004;
        //
        // HotKeyのイベントを示すメッセージID
        //
        const int WM_HOTKEY   = 0x0312;
        //
        // HotKey登録の際に指定するID
        // 解除の際や、メッセージ処理を行う際に識別する値となります。
        //
        // 0x0000～0xbfff 内の適当な値を指定.
        //
        const int HOTKEY_ID_BOSS   = 0x0001;
        const int HOTKEY_ID_ACTIVE = 0x0002;
                                         
        //
        // HotKey登録関数
        //
        // 登録に失敗(他のアプリが使用中)の場合は、0が返却されます。
        //
        [DllImport("user32.dll")]
        extern static int RegisterHotKey(IntPtr hWnd, int id, int modKey, int key) ;

        //
        // HotKey解除関数
        //
        // 解除に失敗した場合は、0が返却されます。
        //
        [DllImport("user32.dll")]
        extern static int UnregisterHotKey(IntPtr HWnd, int ID) ;

       

        public Form1()
        {
            InitializeComponent();
            this.AllowTransparency = false;
            RegisterHotKey(Handle, HOTKEY_ID_BOSS, MOD_SHIFT, (int)Keys.Enter);
            RegisterHotKey(Handle, HOTKEY_ID_ACTIVE, MOD_CONTROL, (int)Keys.Enter);
        }

        /// <summary>
        /// boss key(hot key)
        /// </summary>
        protected override void WndProc(ref Message message)
        {

            base.WndProc(ref message);

            if (message.Msg == WM_HOTKEY)
            {
                switch ((int)message.WParam)
                {
                    //boss key: hide the form
                    case HOTKEY_ID_BOSS:
                        this.Visible = false;
                        this.timer1.Enabled = false;
                        this.timer2.Enabled = false;
                        this.noLabel1.Visible = false;
                        this.word_spellLabel1.Visible = false;
                        this.pronLabel1.Visible = false;
                        this.meaningLabel1.Visible = false;
                        break;
                    case HOTKEY_ID_ACTIVE:
                        if (this.noLabel1.Visible == false)
                        {
                            Reset_Form_Display();
                            this.Visible = true;
                        }
                        this.Activate();
                        break;

                }
         
            }
        }

        private void wordsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.wordsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.greDataSet);

        }

        /// <summary>
        /// Initiation of the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: このコード行はデータを 'greDataSet.words' テーブルに読み込みます。必要に応じて移動、または削除をしてください。
            this.greDataSet.EnforceConstraints = false;
            this.wordsTableAdapter.Fill(this.greDataSet.words);
     
            settings = Settings.ReadSettings();
            this.Location = settings.dispPos == null ? new Point(Screen.PrimaryScreen.WorkingArea.Width / 2 - this.Size.Width / 2, 0) : settings.dispPos;
            Load_Settings();


        }

        /// <summary>
        /// control of auto display of study mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            MoveRecord(true);
            //Turn off the display of meaning
            this.meaningLabel1.Visible = false;
            this.timer2.Enabled = true;
            // Force the form to repaint.
            this.Invalidate();
        }

        /// <summary>
        /// menu control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                 //switch the study mode
                case Keys.M:
                    if(intStudyMode == 1)
                    {
                        intStudyMode = 2;
                        Reset_Form_Display();
                        this.meaningLabel1.Visible = false;
                    }
                    else
                    {
                       intStudyMode = 1;
                       Reset_Form_Display();
                    }
                    break;
                //スペースキー: 停止/開始
                case Keys.Space:
                    if (intStudyMode == 1)
                    {
                        if (this.timer1.Enabled == false)
                        {
                            this.timer1.Enabled = true;
                        }
                        else
                        {
                            this.timer1.Enabled = false;
                        }
                    }
                    else
                    {
                        this.meaningLabel1.Visible = true;
                        this.levelLabel1.Text = "1";
                        this.timer3.Interval = (int)(DEFAULT_INTERVAL_REVIEW_KNOWN * 1000);
                        this.timer3.Enabled = true;
                    }
                    break;
                case Keys.U:
                    if (intStudyMode == 1)
                    {
                        this.levelLabel1.Text = "0";
                    }
                    else if (intStudyMode == 2)
                    {
                        this.meaningLabel1.Visible = true;
                        this.levelLabel1.Text = "0";
                        this.timer3.Interval = DEFAULT_INTERVAL_REVIEW_UNKNOWN * 1000;
                        this.timer3.Enabled = true;
                    }
                    break;
                //boss key
                case Keys.H:
                   
                    break;
                //display the settings form
                case Keys.S:
                    frmSettings = new Form_Settings(this.settings,this);
                    frmSettings.ShowDialog();
                    Load_Settings();
                    break;
                case Keys.Tab:
                    frmRemember = new Form_Remember(this);
                    frmRemember.ShowDialog();
                    break;
                case Keys.ControlKey:
                    this.toolTip_meaning.Hide(this.levelLabel1);
                    blMeaningToolTipDispFlg = false;
                    break;
                //終了させる
                case Keys.E:
                    settings.dispPos = this.Location;
                    settings.no = this.wordsBindingSource.Position;
                    settings.SaveSettings();
                    this.wordsTableAdapter.Update(this.greDataSet);
                    this.Dispose();
                    break;

            }


        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            mousePoint = new Point(e.X, e.Y);
            mousePoint = PointToScreen(mousePoint);
            Cursor.Current = Cursors.SizeAll;

        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            int x, y;
            // 移動したマウスのスクリーン座標を取得
            Point pta = new Point(e.X, e.Y);
            pta = PointToScreen(pta);
            // マウスの左ボタンが押されていて
            // かつ
            // マウスの座標が変化した時
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left &&
                    mousePoint != pta)
            {
                x = this.Location.X + pta.X - mousePoint.X;
                y = this.Location.Y + pta.Y - mousePoint.Y;

                Location = new Point(x, y);
                mousePoint = pta;
            }

        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            Cursor.Current = Cursors.Default;

        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            this.timer2.Enabled = false;
            this.meaningLabel1.Visible = true;
        }

        private void MoveRecord(bool blNext)
        {
            if (blNext)
            {
                if (wordsBindingSource.Position + 1 >= wordsBindingSource.Count)
                {
                    wordsBindingSource.MoveFirst();
                }

                // Otherwise, move back to the first item.
                else
                {
                    wordsBindingSource.MoveNext();
                }

            }
            else
            {
                if (wordsBindingSource.Position - 1 < 0)
                {
                    wordsBindingSource.MoveLast();
                }

                // Otherwise, move back to the first item.
                else
                {
                    wordsBindingSource.MovePrevious();
                }

            }
            this.settings.no = wordsBindingSource.Position + 1;

        }
        private void Reset_Form_Display()
        {
            this.timer1.Enabled = false;
            this.timer2.Enabled = false;
            this.timer3.Enabled = false;
            this.noLabel1.Visible = true;
            this.word_spellLabel1.Visible = true;
            this.pronLabel1.Visible = true;
            this.meaningLabel1.Visible = true;

        }
        private void Transform_Form(string mode)
        {
            int nextComponentXPosition = COMPONENT_INTERVAL;
            if (mode == "office")
            {
                this.Width = OFFICE_FORM_SIZE_WITH;
                this.Height = OFFICE_FORM_SIZE_HEIGHT;
                this.noLabel1.ForeColor = OFFICE_FONT_COLOR;
                this.noLabel1.BorderStyle = BorderStyle.None;
                this.noLabel1.Width = OFFICE_LABLE_NO_SIZE_WITH;
                this.noLabel1.Height = OFFICE_LABLE_NO_SIZE_HEIGHT;
                this.noLabel1.Font = new Font("",OFFICE_FONT_SIZE);
                this.noLabel1.Location = new Point(nextComponentXPosition,OFFICE_COMPONENT_Y_POSITION);
                nextComponentXPosition += this.noLabel1.Width + COMPONENT_INTERVAL;

                this.word_spellLabel1.ForeColor = OFFICE_FONT_COLOR;
                this.word_spellLabel1.BorderStyle = BorderStyle.None;
                this.word_spellLabel1.Width = OFFICE_LABLE_WORD_SIZE_WITH;
                this.word_spellLabel1.Height = OFFICE_LABLE_WORD_SIZE_HEIGHT;
                this.word_spellLabel1.Font = new Font("", OFFICE_FONT_SIZE);
                this.word_spellLabel1.Location = new Point(nextComponentXPosition, OFFICE_COMPONENT_Y_POSITION);
                nextComponentXPosition += this.word_spellLabel1.Width + COMPONENT_INTERVAL;

                this.pronLabel1.ForeColor = OFFICE_FONT_COLOR;
                this.pronLabel1.BorderStyle = BorderStyle.None;
                this.pronLabel1.Width = OFFICE_LABLE_PRON_SIZE_WITH;
                this.pronLabel1.Height = OFFICE_LABLE_PRON_SIZE_HEIGHT;
                this.pronLabel1.Font = new Font("Kingsoft Phonetic Plain", OFFICE_FONT_SIZE);
                this.pronLabel1.Location = new Point(nextComponentXPosition, OFFICE_COMPONENT_Y_POSITION);
                nextComponentXPosition += this.pronLabel1.Width + COMPONENT_INTERVAL;

                this.meaningLabel1.ForeColor = OFFICE_FONT_COLOR;
                this.meaningLabel1.BorderStyle = BorderStyle.None;
                this.meaningLabel1.Width = OFFICE_LABLE_MEANING_SIZE_WITH;
                this.meaningLabel1.Height = OFFICE_LABLE_MEANING_SIZE_HEIGHT;
                this.meaningLabel1.Font = new Font("", OFFICE_FONT_SIZE);
                this.meaningLabel1.Location = new Point(nextComponentXPosition, OFFICE_COMPONENT_Y_POSITION);
                nextComponentXPosition += this.meaningLabel1.Width + COMPONENT_INTERVAL;
                
                this.levelLabel1.ForeColor = OFFICE_FONT_COLOR;
                this.levelLabel1.BorderStyle = BorderStyle.None;
                this.levelLabel1.Width = OFFICE_LABLE_LEVEL_SIZE_WITH;
                this.levelLabel1.Height = OFFICE_LABLE_LEVEL_SIZE_HEIGHT;
                this.levelLabel1.Font = new Font("", OFFICE_FONT_SIZE);
                this.levelLabel1.Location = new Point(nextComponentXPosition, OFFICE_COMPONENT_Y_POSITION);

                this.BackColor = OFFICE_FORM_COLOR;
                this.TransparencyKey = this.BackColor;

            }
            else
            {
                this.Width = HOME_FORM_SIZE_WITH;
                this.Height = HOME_FORM_SIZE_HEIGHT;
                this.noLabel1.ForeColor = HOME_FONT_COLOR;
                this.noLabel1.BorderStyle = BorderStyle.Fixed3D;
                this.noLabel1.Width = HOME_LABLE_NO_SIZE_WITH;
                this.noLabel1.Height = HOME_LABLE_NO_SIZE_HEIGHT;
                this.noLabel1.Font = new Font("", HOME_FONT_SIZE);
                this.noLabel1.Location = new Point(nextComponentXPosition, HOME_COMPONENT_Y_POSITION);
                nextComponentXPosition += this.noLabel1.Width + COMPONENT_INTERVAL;

                this.word_spellLabel1.ForeColor = HOME_FONT_COLOR;
                this.word_spellLabel1.BorderStyle = BorderStyle.Fixed3D;
                this.word_spellLabel1.Width = HOME_LABLE_WORD_SIZE_WITH;
                this.word_spellLabel1.Height = HOME_LABLE_WORD_SIZE_HEIGHT;
                this.word_spellLabel1.Font = new Font("", HOME_FONT_SIZE);
                this.word_spellLabel1.Location = new Point(nextComponentXPosition, HOME_COMPONENT_Y_POSITION);
                nextComponentXPosition += this.word_spellLabel1.Width + COMPONENT_INTERVAL;

                this.pronLabel1.ForeColor = HOME_FONT_COLOR;
                this.pronLabel1.BorderStyle = BorderStyle.Fixed3D;
                this.pronLabel1.Width = HOME_LABLE_PRON_SIZE_WITH;
                this.pronLabel1.Height = HOME_LABLE_PRON_SIZE_HEIGHT;
                this.pronLabel1.Font = new Font("Kingsoft Phonetic Plain", HOME_FONT_SIZE);
                this.pronLabel1.Location = new Point(nextComponentXPosition, HOME_COMPONENT_Y_POSITION);
                nextComponentXPosition += this.pronLabel1.Width + COMPONENT_INTERVAL;

                this.meaningLabel1.ForeColor = HOME_FONT_COLOR;
                this.meaningLabel1.BorderStyle = BorderStyle.Fixed3D;
                this.meaningLabel1.Width = HOME_LABLE_MEANING_SIZE_WITH;
                this.meaningLabel1.Height = HOME_LABLE_MEANING_SIZE_HEIGHT;
                this.meaningLabel1.Font = new Font("", HOME_FONT_SIZE);
                this.meaningLabel1.Location = new Point(nextComponentXPosition, HOME_COMPONENT_Y_POSITION);
                nextComponentXPosition += this.meaningLabel1.Width + COMPONENT_INTERVAL;

                this.levelLabel1.ForeColor = HOME_FONT_COLOR;
                this.levelLabel1.BorderStyle = BorderStyle.Fixed3D;
                this.levelLabel1.Width = HOME_LABLE_LEVEL_SIZE_WITH;
                this.levelLabel1.Height = HOME_LABLE_LEVEL_SIZE_HEIGHT;
                this.levelLabel1.Font = new Font("", HOME_FONT_SIZE);
                this.levelLabel1.Location = new Point(nextComponentXPosition, HOME_COMPONENT_Y_POSITION);

                this.BackColor = HOME_FORM_COLOR;

                this.TransparencyKey = Color.Transparent;

            }

            this.Invalidate();
        }

        private void timer_key_repeat_Tick(object sender, EventArgs e)
        {
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                //前へ
                case Keys.J:
                case Keys.Left:
                    MoveRecord(false);
                    break;
                //次へ
                case Keys.K:
                case Keys.Right:
                    MoveRecord(true);
                    break;
                //display the tooltip for meaning
                case Keys.ControlKey:
                    if (blMeaningToolTipDispFlg == false)
                    {
                        this.toolTip_meaning.Show(this.meaningLabel1.Text, this.levelLabel1);
                        blMeaningToolTipDispFlg = true;
                    }
                    break;

            }
 
        }

        private void wordsBindingSource_BindingComplete(object sender, BindingCompleteEventArgs e)
        {
            if (this.levelLabel1.Text == "0")
            {
                this.noLabel1.ForeColor = Color.Blue;
            }
            else
            {
                this.noLabel1.ForeColor = this.settings.mode=="office"? OFFICE_FONT_COLOR:HOME_FONT_COLOR;
            }
            
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            this.timer3.Enabled = false;
            this.meaningLabel1.Visible = false;
            MoveRecord(true);
        }

        private void Load_Settings()
        {
            if (this.settings.interval <= 0)
            {
                this.timer1.Interval = DEFAULT_INTERVAL * 1000;
            }
            else
            {
                this.timer1.Interval = this.settings.interval * 1000;
            }
            if (this.settings.no > this.wordsBindingSource.Count)
            {
                this.wordsBindingSource.MoveLast();
            }
            else
            {
                this.wordsBindingSource.Position = this.settings.no - 1;
            }

            //Interval of no display of meaning
            if (this.settings.no_disp_interval < 0)
            {
                this.timer2.Interval = DEFAULT_NO_DISP_INTERVAL * 1000;
            }
            else if (this.settings.no_disp_interval > this.settings.interval)
            {
                this.timer2.Interval = this.settings.interval * 1000;
            }
            else
            {
                this.timer2.Interval = this.settings.no_disp_interval * 1000;
            }

            //opacity
            if (this.settings.mode == "home")
            {
                this.Opacity = 1;
            }
            else
            {
                this.Opacity = this.settings.opacity / 100;
            }

            if (this.settings.level == 0)
            {
                dVwordsData = this.greDataSet.words.DefaultView;
                dVwordsData.RowFilter = "level = 0";
                this.wordsBindingSource.DataSource = dVwordsData;
            }
            else
            {
                this.wordsBindingSource.DataSource = this.greDataSet.words;
            }
            
            //transform
            Transform_Form(this.settings.mode);
            this.Invalidate();
        }

       
    }
}
